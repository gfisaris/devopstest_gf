# Module DevOps
class devops {
  # Install Package httpd (Apache)
  package { 'httpd':
    ensure   => installed,
    provider => 'yum',
  }

  # Create Folder /var/www/
  file { '/var/www/':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
  }

  # Create Folder /var/logs/httpd/
  file { '/var/log/httpd/':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  group { 'apache':
    ensure => 'present',
  }

  user { 'apache':
    ensure => 'present',
    groups => 'apache',
  }

  # Create Folder /var/www/devopstest/
  file { '/var/www/devopstest/':
    ensure => 'directory',
    owner  => 'apache',
    group  => 'apache',
    mode   => '0755',
  }

  # Create Folder /var/logs/httpd/devopstest/
  file { '/var/log/httpd/devopstest/':
    ensure => 'directory',
    owner  => 'apache',
    group  => 'apache',
    mode   => '0755',
  }

  # Create and Configure Apache vHost (devopstest) Configuration File
  file {'/etc/httpd/conf.d/devopstest.conf':
    source  => 'puppet:///modules/devops/devopstest.conf',
    require => Package['httpd'],
    notify  => Service['httpd'],
  }

  # Install Package wget
  package { 'wget':
    ensure   => installed,
    provider => 'yum',
  }

  # Retrieve file from S3 Bucket URL using wget
  exec { '/usr/bin/wget https://s3-eu-west-1.amazonaws.com/kaplandevopstest/version.json':
    alias   => 'get-version-json-file',
    cwd     => '/tmp',
    require => Package['wget'],
  }

  # Copy version.json file from /tmp folder over to devopstest vhost folder
  file { '/var/www/devopstest/version.json':
    ensure  => present,
    source  => '/tmp/version.json',
    require => Exec['get-version-json-file'],
    owner   => 'apache',
    group   => 'apache',
  }

  # Run Service httpd (Apache)
  service { 'httpd':
    ensure => 'running',
    enable => true,
  }

}
